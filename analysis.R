#####################################################################################
# Generic function to analyze distribution of Ramanujan numnbrs
#
# Usage: nohup R --no-save < analysis.R
# OR
# R # then
# source("generic_gene_peptide_metadata.R")
#
# Installation:
# install.packages('sqldf')
#
####################################################################################

######################################
# Load required packages
######################################
library(sqldf)

######################################
# Read in file of akk numbers
######################################

# NOTE: file created using
# cat ramanujan*.txt > combined_numbers.txt
# (, ), and done removed
str_filename = 'combined_numbers.txt'
copy_numbers <- read.csv(file = str_filename, sep = ',', 
                         header = FALSE, stringsAsFactors=FALSE, na.strings="..")

copy_numbers$V5

####################################
# select unique numbers
####################################
ramanujan_numbers = sqldf("select distinct(V5) as x from copy_numbers order by x ")

####################################
# plot histogram
####################################
hist(log10(as.numeric(ramanujan_numbers$x)), main="Histogram for Ramanujan numbers", 
     xlab="Log10 of Ramanujan numbers", 
     border="black", 
     col="blue", breaks = 100)


hist((as.numeric(ramanujan_numbers$x)), main="Histogram for Ramanujan numbers", 
     xlab="Ramanujan numbers", 
     border="black", 
     col="blue", breaks = 100)

